FROM gitlab-registry.cern.ch/linuxsupport/cc7-base:latest

MAINTAINER Ricardo Rocha <ricardo.rocha@cern.ch>

RUN echo $'\n\
[eos7-qa] \n\
name=EOS binaries from CERN Koji [qa] \n\
baseurl=http://linuxsoft.cern.ch/internal/repos/eos7-qa/x86_64/os \n\
enabled=1 \n\
gpgcheck=False \n\
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-koji file:///etc/pki/rpm-gpg/RPM-GPG-KEY-kojiv2 \n\
priority=1 \n'\
>> /etc/yum.repos.d/eos7-qa.repo

RUN echo $'\n\
[eos7-qa-debug] \n\
name=EOS binaries from CERN Koji [qa] (debuginfo) \n\
baseurl=http://linuxsoft.cern.ch/internal/repos/eos7-qa/x86_64/debug \n\
enabled=1 \n\
gpgcheck=False \n\
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-koji file:///etc/pki/rpm-gpg/RPM-GPG-KEY-kojiv2 \n\
priority=1 \n'\
>> /etc/yum.repos.d/eos7-qa-debuginfo.repo

RUN yum install -y \
	eos-fusex \
	eos-debuginfo \
	initscripts \
	jq \
	procps-ng \
	&& yum clean all

ADD launch.sh /launch.sh

ENTRYPOINT ["/launch.sh"]
