#!/bin/bash
set -xe

# launch eosxd daemons
unset KRB5CCNAME
unset X509_USER_CERT
unset X509_USER_KEY
ulimit -S -c ${DAEMON_COREFILE_LIMIT:-0}
ulimit -n 65000
test -c /dev/fuse || modprobe fuse
echo starting eosxd

for f in $(ls /etc/eos/fuse.*.conf); do
    mnt=$(basename $f | sed "s/fuse\.\(.*\)\.conf/\1/")
    echo "eosxd: adding fuse mount /eos/${mnt}"
    mkdir -p /eos/${mnt} || true
    umount /eos/${mnt} || true
    /usr/bin/eosxd -o rw,fsname=${mnt} &

    letters=$(cat $f | jq -r ".bind")
    if [ "$letters" != "null" ]
    then
        echo "eosxd: adding bind mounts ${letters} for ${mnt}"
        area=$(cat $f | jq -r ".remotemountdir")
        mkdir -p ${area} || true
        for l in $letters; do
            echo "eosxd: adding symlink for ${area}${l} in /eos/${mnt}/${l}"
            ln -s /eos/${mnt}/${l} ${area}${l} || true
        done
    fi
done

while true; do
    sleep 1000
done
